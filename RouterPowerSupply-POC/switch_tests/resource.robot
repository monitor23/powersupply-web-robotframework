*** Settings ***
Documentation     A resource file with reusable keywords and variables.
...
...               The system specific keywords created here form our own
...               domain specific language. They utilize keywords provided
...               by the imported SeleniumLibrary.
Library           SeleniumLibrary

*** Variables ***
${DDWRT_SERVER}         192.168.5.65
${POWER_SUPPLY_SERVER}         192.168.5.48
${BROWSER}        Firefox
${DELAY}          6
${DDWRT_VALID USER}     root
${DDWRT_VALID PASSWORD}    Venus2009!
${DDWRT_LOGIN URL}      http://${DDWRT_SERVER}/
${POWER_SUPPLY_LOGIN URL}      http://${POWER_SUPPLY_SERVER}/
${DDWRT_WELCOME URL}    http://${DDWRT_SERVER}/cgi-bin/luci/
${POWER_SUPPLY_WELCOME_URL}     http://${POWER_SUPPLY_SERVER}/login.html
${DDWRT_ERROR URL}      http://${DDWRT_SERVER}/cgi-bin/luci/
${POWER_SUPPLY_ERROR URL}     http://${POWER_SUPPLY_SERVER}/login.html
${POWER_SUPPLY_VALID PASSWORD}    adsl55

*** Keywords ***
Open Browser To DDWRT Router Login Page
    Open Browser    ${DDWRT_LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    DDWRT Login Page Should Be Open

DDWRT Login Page Should Be Open
    Reload Page
    Set Selenium Speed    ${DELAY}
    Wait Until Element Is Visible   luci_username
    Title Should Be     LEDE_UK - Overview - LuCI

Open Browser To Power Supply Login Page
    Open Browser    ${POWER_SUPPLY_LOGIN URL}    ${BROWSER}
    Maximize Browser Window
    Set Selenium Speed    ${DELAY}
    Power Supply Login Page Should Be Open

Power Supply Login Page Should Be Open
    Wait Until Element Is Visible   pw
    Title Should Be     EnerGenie LAN Power Manager

Input Username
    Wait Until Element Is Visible   luci_username
    [Arguments]    ${username}
    Input Text    luci_username    ${username}

Input Password
    [Arguments]    ${password}
    Input Text    luci_password    ${password}

Input Power Supply Password
    [Arguments]    ${password}
    Input Text    pw    ${password}

Submit Credentials
    Click Button    //input[@value='Login']

Submit Power Supply Credentials
    Click Button    //input[@value='Enter']

DDWRT Welcome Page Should Be Open
    Location Should Be    ${DDWRT_WELCOME URL}
    Title Should Be    LEDE_UK - Overview - LuCI

Power Supply Welcome Page Should Be Open
    Location Should Be    ${POWER_SUPPLY_WELCOME_URL}
    Title Should Be    EnerGenie LAN Power Manager

Click Wireless Option
    Set Selenium Speed    ${DELAY}
    Wait Until Element is Visible   //a[contains(text(),'Network')]
    Click Link  //a[contains(text(),'Network')]
    Set Selenium Speed    ${DELAY}
    Wait Until Element is Visible   //a[contains(text(),'Wireless')]
    Click Link  //a[contains(text(),'Wireless')]

Disbale The SSID_OpenWrt Wifi
    Set Selenium Speed    ${DELAY}
    Wait Until Element is Visible   //div[@id='cbi-wireless-default_radio0']/div[3]/div/button
    Element Text Should Be    //div[@id='cbi-wireless-default_radio0']/div[3]/div/button    Disable
    Click Element  //div[@id='cbi-wireless-default_radio0']/div[3]/div/button
    Reload Page

Enable The SSID_OpenWrt Wifi
    Set Selenium Speed    ${DELAY}
    Wait Until Element is Visible   //div[@id='cbi-wireless-default_radio0']/div[3]/div/button
    Element Text Should Be    //div[@id='cbi-wireless-default_radio0']/div[3]/div/button    Enable
    Click Element  //div[@id='cbi-wireless-default_radio0']/div[3]/div/button
    Reload Page

Turn Off Socket B
    Set Selenium Speed    ${DELAY}
    Wait Until Element is Visible   (//a[contains(text(),'OFF')])[2]
    Element Text Should Be  (//a[contains(text(),'OFF')])[2]    OFF
    Click Element   (//a[contains(text(),'OFF')])[2]
    Wait Until Element is Visible   (//a[contains(text(),'OFF')])[2]

Turn ON Socket B
    Set Selenium Speed    ${DELAY}
    Wait Until Element is Visible   //span[@id='stCont1']/a
    Element Text Should Be  //span[@id='stCont1']/a    ON
    Click Element   //span[@id='stCont1']/a
    Wait Until Element is Visible   //span[@id='stCont1']/a
    log to console  1min wait for ddwrt portal
    Set Selenium Speed    25

Logout From DDWRT Portal
    Wait Until Element is Visible   //a[contains(text(),'Logout')]
    Click Element   //a[contains(text(),'Logout')]

Logout From Power Supply Portal
    Wait Until Element is Visible   //a[contains(text(),'Log Out')]
    Click Element   //a[contains(text(),'Log Out')]