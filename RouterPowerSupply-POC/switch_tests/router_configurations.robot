*** Settings ***
Documentation     A test suite with Router Configuration Scenarios
...
...               This test has a workflow that is created using keywords in
...               the imported resource file.
Resource          resource.robot

*** Test Cases ***
Disbale SSID_OpenWrt Wifi Configuration
    Open Browser To DDWRT Router Login Page
    Input Username    ${DDWRT_VALID USER}
    Input Password    ${DDWRT_VALID PASSWORD}
    Submit Credentials
    DDWRT Welcome Page Should Be Open
    Set Selenium Speed    ${DELAY}
    Click Wireless Option
    Set Selenium Speed    ${DELAY}
    #    Going to Disable SSID: OpenWrt BSSID: 54:64:D9:03:0A:83 Router
    Disbale The SSID_OpenWrt Wifi
    Set Selenium Speed    ${DELAY}
    Logout From DDWRT Portal
    [Teardown]    Close Browser

Turn OFF Socket B Power Supply
    Open Browser To Power Supply Login Page
    Input Power Supply Password    ${POWER_SUPPLY_VALID PASSWORD}
    Submit Power Supply Credentials
    Power Supply Welcome Page Should Be Open
    Turn Off Socket B
    Logout From Power Supply Portal
    [Teardown]    Close Browser

Check DDWRT Server Not Accessible
    log to console  'There should be connection time out failure.'
    Open Browser To DDWRT Router Login Page
    [Teardown]    Close Browser

Turn ON Socket B Power Supply
    Open Browser To Power Supply Login Page
    Input Power Supply Password    ${POWER_SUPPLY_VALID PASSWORD}
    Submit Power Supply Credentials
    Power Supply Welcome Page Should Be Open
    Turn ON Socket B
    Logout From Power Supply Portal
    [Teardown]    Close Browser

Enable SSID_OpenWrt Wifi Configuration
    Open Browser To DDWRT Router Login Page
    Input Username    ${DDWRT_VALID USER}
    Input Password    ${DDWRT_VALID PASSWORD}
    Submit Credentials
    DDWRT Welcome Page Should Be Open
    Set Selenium Speed    ${DELAY}
    Click Wireless Option
    Set Selenium Speed    ${DELAY}
    #   Going to Enable SSID: OpenWrt BSSID: 54:64:D9:03:0A:83 Router
    Enable The SSID_OpenWrt Wifi
    Set Selenium Speed    ${DELAY}
    Logout From DDWRT Portal
    [Teardown]    Close Browser